// Style
import '../style/layout.scss';
// Third parties
import './vendor';
// Class
import { FishSelectionManager } from './class/fish-selection';
import { FishShopService } from './class/fish-shop-service';
import { FishCart } from './class/fish-cart';

(function(){    
    document.addEventListener('DOMContentLoaded', event => {
        var Fishservice = new FishShopService();
        var fishCart = new FishCart( {fishService: Fishservice} );
        var fishListManager = new FishSelectionManager( {fishCartComponent: fishCart} );
    });
})();