export class FishShopService {
    
    url: string = 'https://fishshop.attest.tech/compatibility';
    
    constructor() {
        
    }

    checkCompatibility(body: {}): Promise<Response> {
        return fetch(this.url, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify(body)
        }).then( value => {
            return value.json();
        }, err => {
            return err;
        }).catch( reason => {
            return reason;
        });
    }
}