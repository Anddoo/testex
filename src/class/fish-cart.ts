import { FishShopService } from './fish-shop-service';

export class FishCart {

    // Properties
    private fishsInCart: string[] = [];
    private fishObject: {} = {};
    private fishService: FishShopService;

    // Properties use to keep a state of the app
    private lastAction: string;
    private cartIsBusy: boolean = false;

    // Dom element
    private fishSelectedUl: HTMLUListElement = <HTMLUListElement> document.querySelector('.cart ul');
    private fishBoughtUl: HTMLDivElement = <HTMLDivElement> document.querySelector('.fish-picked ul');
    
    private resetBtn: HTMLButtonElement = <HTMLButtonElement> document.querySelector('.reset-btn');
    private buyBtn: HTMLButtonElement = <HTMLButtonElement> document.querySelector('.buy-btn');
    
    private errorMessage: HTMLDivElement = <HTMLDivElement> document.querySelector('.error-message');

    // Methods
    constructor(parameters: any) {
        this.init(parameters);
    };

    // Public 
    
    // This method is used from the fish-selection class in order to know if the cart is in the middle of processing something.
    // If that's the case he simply prevent user to add more fish.
    public isCartBusy(): boolean {
        return this.cartIsBusy;
    };

    // Private

    private init(parameters: any): void {
        this.fishService = parameters.fishService;
        this.bind();
        this.displaySelectedFish();
    };

    private bind(): void {
        this.resetBtn.addEventListener('click', event => {
            this.emptyCart();
            this.errorMessage.innerHTML = '';
        });

        this.buyBtn.addEventListener('click', event => {
            this.emptyTank();
            this.displayBoughtFish()
        });
    };

    public addFish(name: string): void {
        this.cartIsBusy = true;
        this.lastAction = '+';
        this.errorMessage.innerHTML = '';

        this.fishsInCart.push(name)

        this.computeCart();
        this.displaySelectedFish();
        this.checkCompatibility();
    };

    public removeFish(name: string): void {
        // Prevent user to do anything while waiting for response from server.
        if(this.cartIsBusy) {
            return;
        }

        this.cartIsBusy = true;
        this.lastAction = '-';

        //Check if this fish is in the cart
        let index = this.fishsInCart.indexOf(name);
        if(index !== -1) {
            this.fishsInCart.splice(index, 1);
        } else {
            //do nothing
            return;
        }

        this.computeCart();
        this.displaySelectedFish();
        this.checkCompatibility();
    };
    
    private displaySelectedFish(): void {
        //Reset Cart visually
        while (this.fishSelectedUl.firstChild) {
            this.fishSelectedUl.removeChild(this.fishSelectedUl.firstChild);
        }

        let docfrag = document.createDocumentFragment();
        if(Object.keys(this.fishObject).length !== 0) {
            for (var fish in this.fishObject) {
                var li = document.createElement("li");
                li.textContent = fish + ' : ' + this.fishObject[fish];
                
                var btn = document.createElement("button");
                btn.setAttribute('fish-name', fish);
                btn.innerHTML = "-";
                btn.addEventListener('click', event => {
                    let element = <HTMLElement> event.target;
                    this.removeFish( element.getAttribute('fish-name')  );
                });

                li.appendChild(btn);
                docfrag.appendChild(li);
            }
        } else  {
            var li = document.createElement("li");
            li.textContent = 'The cart is empty';
            docfrag.appendChild(li);
        }
        this.fishSelectedUl.appendChild(docfrag);
    };

    private displayBoughtFish(): void {
        let docfrag = document.createDocumentFragment();
        if(Object.keys(this.fishObject).length !== 0) {
            for (var fish in this.fishObject) {
                var li = document.createElement("li");
                li.textContent = this.fishObject[fish] + ' ' + fish;
                docfrag.appendChild(li);
            }
        } else  {
            var li = document.createElement("li");
            li.textContent = 'Congrats you just buy ... nothing try again.';
            docfrag.appendChild(li);
        }
        this.fishBoughtUl.appendChild(docfrag);
    };

    private computeCart(): void {
        this.fishObject = {};

        this.fishsInCart.forEach( fish => {
            this.fishObject[fish] = (this.fishObject[fish] === undefined ? 0 : this.fishObject[fish] )+1
        });

    };

    private emptyCart(): void {
        this.fishsInCart.length = 0;
        this.fishObject = {};

        this.displaySelectedFish();
    };
    
    private emptyTank(): void {
        while (this.fishBoughtUl.firstChild) {
            this.fishBoughtUl.removeChild(this.fishBoughtUl.firstChild);
        }
    };

    private checkCompatibility(): void {
    
        let arrayWithUniqueValue = this.fishsInCart.filter( (value, index, self) => { return self.indexOf(value) === index; })

        var deferred = this.fishService.checkCompatibility( {"fish": arrayWithUniqueValue} );
        deferred.then( value => {
            let result = <any> value;
            // in this case there is a chance this is our 429 status Code.
            if(this.handle429StatusCode(result)){
                this.cartIsBusy = false;
                return;
            }
            // We receive an error like a 500 one for example.
            if(this.handleErrorResponseFromRequest(result)){
                return;
            }
            // Everything seems alright display information.
            this.errorMessage.innerHTML = (result.canLiveTogether) ? 'Fish Compatible' : 'You shouldn\' buy thoses fish together';
            this.buyBtn.disabled = !result.canLiveTogether;
            this.cartIsBusy = false;
        }, err => {
            console.log(err);
        }).catch( reason => {
            console.log(reason);
        });
    };

    private handle429StatusCode(httpResponse: any): boolean {
        if( httpResponse.canLiveTogether === undefined && httpResponse.errorMessage === undefined ) {
            this.errorMessage.innerHTML = httpResponse.stack;
            if(this.lastAction === '+') {
                this.fishsInCart.pop();
            }
            return true;
        } else {
            return false;
        }
    };

    private handleErrorResponseFromRequest(httpResponse: any): boolean {
        if(httpResponse.errorMessage) {
            // Since the Api return something wrong we remove the fish from the cart and just warn the user.
            this.errorMessage.innerHTML = httpResponse.errorMessage + '<br> You should retry.';
            if(this.lastAction === '+') {
                this.fishsInCart.pop();
            }
            this.computeCart();
            this.displaySelectedFish();
            this.cartIsBusy = false;
            return true;
        } else {
            return false;
        }
    };

}