import { listOfFish } from '../data-fish';
import { FishCart } from './fish-cart';

export class FishSelectionManager {

    private listOfFish: string[];
    private fishUl: HTMLUListElement = <HTMLUListElement> document.querySelector('.fish-list ul');

    private fishCart: FishCart;
    
    constructor(parameters: any) {
        this.init(parameters);
    };
    
    private init(parameters: any): void  {
        this.listOfFish = listOfFish;
        this.fishCart = parameters.fishCartComponent;
        this.buildFishList();
    };

    private buildFishList(): void {
        let docfrag = document.createDocumentFragment();

        this.listOfFish.forEach( fish => {
            var li = document.createElement("li");
            li.addEventListener('click', event => {
                let name =  event.target as HTMLElement;
                this.addFish(name.textContent);
            })
            li.textContent = fish;
            docfrag.appendChild(li);
        });

        this.fishUl.appendChild(docfrag);
    };

    private addFish(name: string): void {
        if(!this.fishCart.isCartBusy()) {
            this.fishCart.addFish(name);
        }
    };
};